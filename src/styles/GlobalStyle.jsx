import {device, colors} from './Variables';
import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  
  body {
    margin: 10px;
    background-color:${props => props.theme.bgColor};
    font-family: ${props => props.theme.fontFamily};
    margin-bottom:50px;

    @media ${device.tablet} {
      /* background-color:${colors.green};; */
    }

    @media ${device.laptop} {
      /* background-color:${colors.salmon}; */
    }

    @media ${device.laptopL} {
      /* background-color:${colors.yellow};; */
    }
    
  }
`;