import styled from 'styled-components';
import { colors , device} from './Variables';

import Modal, { BaseModalBackground } from "styled-react-modal";
import { animated } from "react-spring";

export const NavBar = styled(animated.nav)`
  border-bottom:1px solid ${colors.greyLightBorder};
  margin:0 5%;
`;

export const FlexContainer = styled.div`
  max-width: 120rem;
  display: flex;
  margin: auto;
  padding: 0 2rem;;
  justify-content: space-between;
  height: 5rem;
`;

// NAVBAR

export const NavLinksDesktop = styled(animated.div)`
  justify-self: end;
  list-style-type: none;
  margin-top:25px;
  
  & a {
    color: ${colors.green};
    text-transform: uppercase;
    font-weight: 400;
    margin: 0 0.5rem;
    font-size:20px;
    transition: all 300ms linear 0s;
    text-decoration: none;
    cursor: pointer;
    &:hover {
      color:  ${colors.yellow};
    }
    @media (max-width: 768px) {
      display: none;
    }
  }
`;
 
export const BurgerWrapper = styled.div`
  margin: 10px -30px;
  @media (min-width: 769px) {
    display: none;
  }
`;


// BURGER

export const Wrapper = styled.div`
  position: relative;
  padding-top: .7rem;
  cursor: pointer;
  display: block;
  & span {
    background:  ${colors.green};
    display: block;
    position: relative;
    width: 26px;
    height: 2px;
    margin-bottom: 7px;
    transition: all ease-in-out 0.2s;
  }
  .open span:nth-child(2) {
      opacity: 0;
    }
  .open span:nth-child(3) {
    transform: rotate(45deg);
    top: -6px;
  }
  .open span:nth-child(1) {
    transform: rotate(-45deg);
    top: 11px;
  }
`;

export const CollapseWrapper = styled(animated.div)`
  background:  ${colors.green};
  position: fixed;
  top: 5.5rem;
  left: 0;
  right: 0;
`;

export const NavLinksMobile = styled.div`
  list-style-type: none;
  padding: 2.5rem 1rem 2rem 2rem;
  display: flex;
  flex-direction: column;
  
  & a {
    font-size: 1.4rem;
    line-height: 2;
    color: #dfe6e9;
    text-transform: uppercase;
    text-decoration: none;
    cursor: pointer;
    &:hover {
      color:  ${colors.yellow};;
    }
  }
`;

export const TitleSiteH2 = styled.h2`
  div{
    text-align:center;
    margin:0 3%;
  }
    h2{
      font-weight:100;
      letter-spacing: -4px;
      color:${colors.green};
      font-size: 55px;
      word-break:breck;
      line-height:55px;
      span{
        font-weight:bolder;
        font-weight:800;
      }

      @media ${device.mobileL} {
        font-size: 48px;
      }
    }
`;

export const MessageSiteP = styled.div`
  p{
    text-align:center;
    margin:0 10%;
    color:${colors.grey};
    font-size: 35px;
    font-weight: 100;
    letter-spacing: -3px;
    line-height:45px;
    span{
      height: 25px;
      width: 25px;
      background-color: #bbb;
      border-radius: 50%;
      display: inline-block;
      margin-left:10px;
      &:nth-child(2){
        background-color:${colors.green}
      }
      &:nth-child(3){
        background-color:${colors.grey}
      }
      &:nth-child(4){
        background-color:${colors.salmon}
      }
      &:nth-child(5){
        background-color:${colors.yellow}
      }
      &:nth-child(6){
        background-color:${colors.purple}
      }
    }
    &:nth-child(2){
      margin:0 auto;
      @media ${device.tablet} {
        &:nth-child(2){
          width:350px
        }
      }
      @media ${device.mobileL} {
        &:nth-child(2){
          width: 190px;
        }
      }
    }
  }

`;

// FEATURES
export const FeaturesSite = styled.div`
    display: flex;
    flex-direction: row;
    align-content: space-between;
    justify-content: space-between;
    margin-top:30px;

    @media ${device.mobileL} {
      flex-direction: column;
      align-content: initial;
      justify-content: initial;
      padding:10px;
    }
  div{
    width:31vw;
    background-color:${colors.white};
    display:block;

    @media ${device.mobileL} {
      width:100%;
      margin:0px;
      color: blue;
      margin-bottom: 10px;
    }
  }
`;

const featureBoxParentStyle = {
  text : `
  height:auto;
  `
}

const featureBoxStyle = {
  text : `
  height:auto;
  color:${colors.white};
  text-align:center;
  letter-spacing: -3px;
  font-weight: 100;
  font-size: 29px;
  margin-botton:20px;
    p{margin:0px; padding:10px;}

  `
}

const featureBoxBelowStyle ={
  text:` 
  &:nth-child(2){
    background-color:white;
    color:grey;
    font-size:20px;
    letter-spacing:initial;
    text-align:left;
    `
}

export const BoxOne = styled.div`
  ${featureBoxParentStyle.text}
  div{
    background-color:${colors.salmon};
    ${featureBoxStyle.text};
    ${featureBoxBelowStyle.text};
  }
`;
export const BoxTwo = styled.div`
  ${featureBoxParentStyle.text}
  div{
    background-color:${colors.yellow};
    ${featureBoxStyle.text}
    ${featureBoxBelowStyle.text};
  }
`;
export const BoxTree = styled.div`
  ${featureBoxParentStyle.text}
  div{
    background-color:${colors.purple};
    ${featureBoxStyle.text}
    ${featureBoxBelowStyle.text};
  }
`;

export const Image = styled.img`
    width: 100%;
    height: auto;
    
`;


export const Button = styled.button`
  &.first_bt{
    background: ${colors.salmon};
    border: 2px solid ${colors.salmon};
  }
  &.sec_bt{
    background: ${colors.yellow};
    border: 2px solid ${colors.yellow};
  }
  background: ${colors.purple};
  color: ${colors.white};

  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid ${colors.purple};
  border-radius: 3px;
`;


// MODAL


export const StyledModal = Modal.styled`
  width: 20rem;
  height: 20rem;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
  opacity: ${props => props.opacity};
  transition: opacity ease 500ms;
 
`;

export const FadingBackground = styled(BaseModalBackground)`
opacity: ${props => props.opacity};
transition: opacity ease 200ms;
`;


export const Close = styled.div`
  height: 50px;
  width: 50px;
  background-color: #fff0;
  border-radius: 50%;
  text-align: center;
  color: #ececec;
  position: absolute;
  top: 60px;
  font-size: 32px;
  border: solid;
`