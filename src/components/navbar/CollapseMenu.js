import React from 'react';
import Links from "./Links";
import { CollapseWrapper, NavLinksMobile} from '../../styles/Style';

import { useSpring } from 'react-spring';

const CollapseMenu = (props) => {
  const { open } = useSpring({ open: props.navbarState ? 0 : 1 });

  if (props.navbarState === true) {
    return (
      <CollapseWrapper style={{
        transform: open.interpolate({
          range: [0, 0.2, 0.3, 1],
          output: [0, 0, 0, -200],
        }).interpolate(openValue => `translate3d(0, ${openValue}px, 0`),
      }}
      >
        <NavLinksMobile>
            <Links/>
        </NavLinksMobile>
      </CollapseWrapper>
    );
  }
  return null;
};

export default CollapseMenu;

