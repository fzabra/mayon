import React from 'react'
import { TitleSiteH2 } from '../styles/Style';

const TitleSite = () => {
  return (
    <TitleSiteH2>
      <div>
        <h2>Este Site é <span>responsivo</span> com <span>REACT</span> utilizando <span>sytled-components</span></h2>         
      </div>
    </TitleSiteH2>
  );
}

export default TitleSite;
