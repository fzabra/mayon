import React from 'react'

import { MessageSiteP } from '../styles/Style';


const MessageSite = () => {
  return (
    <MessageSiteP>
      <p>
        A fonte utilizada é a Open Sans de 100 a 800.
        exemplo: "Open Sans", Helvetica, sans-serif, arial;      
      </p>
      <p>
        Já as cores são:<br/>
        <span></span>#007F56;
        <span></span>#868686;
        <span></span>#FE9481;
        <span></span>#FCDA92;
        <span></span>#9C8CB9;  
      </p>
    </MessageSiteP>
  );
}

export default MessageSite;
