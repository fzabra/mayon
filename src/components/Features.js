import React, { useState} from 'react'

import { FeaturesSite, Button, BoxOne, BoxTwo, BoxTree , Image, StyledModal, Close} from '../styles/Style';
import mobile from '../images/mobile.png';
import tablet from '../images/tablet.png';
import desktop from '../images/desktop.png';

function FancyModalButton() {
  const [isOpen, setIsOpen] = useState(false);
  const [opacity, setOpacity] = useState(0);

  function toggleModal(e) {
    setIsOpen(!isOpen);
  }

  function afterOpen() {
    setTimeout(() => {
      setOpacity(1);
    }, 10);
  }

  function beforeClose() {
    return new Promise(resolve => {
      setOpacity(0);
      setTimeout(resolve, 200);
    });
  }

  return (
    <div>
      <Button className="sec_bt" onClick={toggleModal}>Leia Mais</Button>
      <StyledModal
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <span>I am a modal!</span>
        <Close onClick={toggleModal}>x</Close>
      </StyledModal>
    </div>
  );
}

const Features = (props) => {

  const handleClickMobile = () => {
    props.cleanAll();
  }

    return (
      <FeaturesSite>
        <BoxOne>
          <div>
            <p><Image src={desktop} alt="Company Logo" /></p>
            <p>Site Responsivo DESKTOP</p>
          </div>
          <div>
            <p>Quando pressionado o botão <b>Leia mais...</b> o restante da informação deverá aparecer em scroll down.</p>
            <Button className="first_bt">Leia Mais</Button>
          </div>
        </BoxOne>
        <BoxTwo>
          <div>
              <p><Image src={tablet} alt="Company Logo" /></p>
              <p>Site Responsivo TABLET</p>
          </div>
          <div>
            <p>Quando pressionado o botão <b>Leia mais...</b> o restante da informação deverá aparecer em scroll down.</p>
            <FancyModalButton />
          </div>
        </BoxTwo>
        <BoxTree>
          <div>
              <p><Image src={mobile} alt="Company Logo" /></p>
              <p>Site Responsivo MOBILE</p>
          </div>
          <div>
            <p>Quando pressionado o botão <b>Alterar Tema...</b> modifique o tema do site para blackfriday a seu gosto.</p>
            <Button onClick={handleClickMobile}>Alterar Tema</Button>
          </div>   
        </BoxTree>
      </FeaturesSite>
    );
  
}

export default Features;
