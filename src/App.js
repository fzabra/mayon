import React from 'react'
import { GlobalStyle } from './styles/GlobalStyle';
import Navbar from "./components/navbar/Navbar";
import TitleSite from './components/TitleSite';
import MessageSite from './components/MessageSite';
import Features from './components/Features';
import { ThemeProvider, withTheme } from 'styled-components';
import { ModalProvider } from "styled-react-modal";
import { colors } from './styles/Variables';
import { FadingBackground } from './styles/Style';

const App = () => {
  const [navbarOpen, setNavbarOpen] = React.useState(false);
  const [isNormal, setIsNormal] = React.useState(true);
  const [color, setColor] = React.useState(colors.greyLight);
  const [font, setFont] = React.useState("'Open Sans', sans-serif");

  const theme = withTheme({
    bgColor: color,
    fontFamily: font,
  });

  const handleNavbar = () => {
    setNavbarOpen(!navbarOpen);
  }


  const handleClick = () => {
    setIsNormal(!isNormal);
    setColor(!isNormal ? colors.greyLight : colors.blackFriday);
    setFont(!isNormal ? "'Open Sans', sans-serif": "'Roboto', sans-serif");
  }

  return (
    <ThemeProvider theme={theme}>
      <ModalProvider backgroundComponent={FadingBackground}>

        <Navbar
          navbarState={navbarOpen}
          handleNavbar={handleNavbar}
        />
          <TitleSite />
        <MessageSite />
        <Features cleanAll={handleClick} />
        <GlobalStyle />
      </ModalProvider>
    </ThemeProvider>
  )

}

export default App;